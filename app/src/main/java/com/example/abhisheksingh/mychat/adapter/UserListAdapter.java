package com.example.abhisheksingh.mychat.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.abhisheksingh.mychat.MainActivity;
import com.example.abhisheksingh.mychat.MessagingActivity;
import com.example.abhisheksingh.mychat.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhishek Singh on 3/28/2018.
 */

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {
    Context context;
    List<String> list = new ArrayList<>();

    public UserListAdapter(Context baseContext, List<String> list) {
        context = baseContext;
        this.list = list;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_users, null);
//        return new ViewHolder(view);
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_users, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.userName.setText(list.get(position).toString());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView userName;

        public ViewHolder(View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.tvUserName);
            userName.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tvUserName:
//                    if(context instanceof MainActivity){
//                        ((MainActivity)context).moveToNextScreen(list.get(getAdapterPosition()).toString());
                    Intent intent = new Intent(context, MessagingActivity.class);
                    intent.putExtra("id", list.get(getAdapterPosition()).toString());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    //  }
                    break;
            }

        }
    }
}

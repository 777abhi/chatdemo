package com.example.abhisheksingh.mychat.adapter;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.abhisheksingh.mychat.MessagingActivity;
import com.example.abhisheksingh.mychat.MySharepref;
import com.example.abhisheksingh.mychat.R;
import com.example.abhisheksingh.mychat.Utils.Constants;
import com.example.abhisheksingh.mychat.pojo.Chat;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhishek Singh on 3/28/2018.
 */

public class MessaginAdapter extends RecyclerView.Adapter<MessaginAdapter.ViewHolder> {
    Context context;
    List<Chat> list;
MySharepref mySharepref;
    private MediaPlayer mediaPlayer;
    public MessaginAdapter(Context baseContext, List<Chat> list) {
        context = baseContext;
        this.list = list;
        mySharepref= new MySharepref(context);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_message, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        if (list.get(position).getMessagetype().equals(Constants.TEXT)) {
        long time = Long.parseLong(list.get(position).getTimestamp());
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm ");
        String date = sdf.format(time);

            switch (list.get(position).getMessagetype())
            {
                case Constants.TEXT:

                    if(list.get(position).getUserid().equals(mySharepref.getUserId()))
                    {
                        holder.rlRight.setVisibility(View.VISIBLE);
                        holder.rlRight.requestLayout();
                        holder.rlLeft.setVisibility(View.GONE);
                        holder.ivImageRIght.setVisibility(View.GONE);
                        holder.ivImageLft.setVisibility(View.GONE);
                       // holder.ivImage.setVisibility(View.GONE);

                        holder.tvMessageRight.setText(list.get(position).getMessage());

                        holder.tvTimeRight.setText(date);
                    }
                    else{
                        holder.rlRight.setVisibility(View.GONE);
                        holder.rlLeft.setVisibility(View.VISIBLE);
                        holder.rlLeft.requestLayout();
                        holder.ivImageRIght.setVisibility(View.GONE);
                        holder.ivImageLft.setVisibility(View.GONE);
                   // holder.ivImage.setVisibility(View.GONE);
                    holder.tvMessageLeft.setText(list.get(position).getMessage());
                    holder.tvTimeLeft.setText(date);}
                    break;
                case Constants.IMAGE:
                    if(list.get(position).getUserid().equals(mySharepref.getUserId())) {
                        holder.ivImageLft.setVisibility(View.VISIBLE);
                        holder.ivImageLft.requestLayout();
                        holder.rlAudio.setVisibility(View.GONE);
                        holder.ivImageRIght.setVisibility(View.GONE);
                        Glide.with(context).load(list.get(position).getVideoImage()).into(holder.ivImageLft);
                    }
                    else
                    {
                        holder.ivImageLft.setVisibility(View.GONE);
                        holder.ivImageRIght.setVisibility(View.VISIBLE);
                        holder.rlAudio.setVisibility(View.GONE);
                        holder.ivImageRIght.requestLayout();
                        Glide.with(context).load(list.get(position).getVideoImage()).into(holder.ivImageRIght);
                    }
                    break;
                case Constants.AUDIO:
                    if(list.get(position).getUserid().equals(mySharepref.getUserId())) {
                        holder.rlAudio.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        holder.rlAudio.setVisibility(View.VISIBLE);
                    }
                    break;
//                    holder.ivImage.setVisibility(View.VISIBLE);
//                    holder.tvMessage.setVisibility(View.GONE);
//                    Glide.with(context).load(list.get(position).getVideoImage()).into( holder.ivImage);
//

            }

        }

//         if(list.get(position).getMessagetype().equals(Constants.IMAGE)) {
//
//            holder.ivImage.setVisibility(View.VISIBLE);
//            Glide.with(context).load(list.get(position).getVideoImage()).into( holder.ivImage);
//        }
//        }



    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,MediaPlayer.OnCompletionListener{
        TextView tvMessageRight, tvTimeRight,tvMessageLeft,tvTimeLeft;
        ImageView ivImageLft,ivImageRIght ,ivPLay;
        RelativeLayout rlRight,rlLeft,rlAudio;

        public ViewHolder(View itemView) {
            super(itemView);
            rlAudio=itemView.findViewById(R.id.rlAudio);

            rlRight=itemView.findViewById(R.id.rlRight);
            tvMessageRight = itemView.findViewById(R.id.tvMessageRight);
            tvTimeRight = itemView.findViewById(R.id.tvTimeRight);
            ivPLay=itemView.findViewById(R.id.ivPLay);

            rlLeft=itemView.findViewById(R.id.rlLeft);
            tvMessageLeft = itemView.findViewById(R.id.tvMessageLeft);
            tvTimeLeft = itemView.findViewById(R.id.tvTimeLeft);
            ivImageRIght=itemView.findViewById(R.id.ivImageRIght);
            ivImageLft=itemView.findViewById(R.id.ivImageLft);
          //  ivImage=itemView.findViewById(R.id.ivImage);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId())
            {
                case R.id.ivPLay:
                    try {
                        mediaPlayer.setDataSource(list.get(getAdapterPosition()).getVideoImage()); // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
                        mediaPlayer.prepare();
                        ivPLay.setImageResource(R.drawable.ic_action_playback_pause);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    break;
            }

        }

        @Override
        public void onCompletion(MediaPlayer mp) {
            ivPLay.setImageResource(R.drawable.ic_action_playback_play);
        }
    }
}

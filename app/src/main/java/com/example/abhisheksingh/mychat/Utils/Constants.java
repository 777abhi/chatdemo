package com.example.abhisheksingh.mychat.Utils;

/**
 * Created by Abhishek Singh on 3/29/2018.
 */

public class Constants {
    public static final String TEXT="TEXT";
    public static final String IMAGE="IMAGE";
    public static final String AUDIO="AUDIO";
    public static final String VIDEO="VIDEO";
}

package com.example.abhisheksingh.mychat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {
@BindView(R.id.etUser)
EditText etUser;
List<String> list=new ArrayList<>();
MySharepref mySharepref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mySharepref=new MySharepref(this);
        init();
    }
    public void init()
    {
        list.add("abhi_1");
        list.add("asd_3");
        list.add("sachy_2");
    }
    @OnClick(R.id.btLogin)
            public void click()
    {
        String user=etUser.getText().toString();
        if(!user.isEmpty())
        {
 if(list.contains(user))
 {
mySharepref.setUserId(user);
     Intent intent=new Intent( LoginActivity.this,MainActivity.class);
     startActivity(intent);
 }
        }
    }
}

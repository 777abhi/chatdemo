package com.example.abhisheksingh.mychat.pojo;

/**
 * Created by Abhishek Singh on 3/28/2018.
 */

public class User {
    String id;
    String username;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

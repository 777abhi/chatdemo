package com.example.abhisheksingh.mychat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.abhisheksingh.mychat.adapter.UserListAdapter;
import com.example.abhisheksingh.mychat.pojo.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    FirebaseDatabase database;
    DatabaseReference myRef;
    DatabaseReference userRef;
    private String TAG="TAG";
    List<String> list;
@BindView(R.id.rcUserList)
    RecyclerView rcUserList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        database = FirebaseDatabase.getInstance();
        ButterKnife.bind(this);
        list=new ArrayList<>();
        readUser();

//        intialize();
    }
    public void intialize()
    {
        // Write a message to the database

         myRef = database.getReference("users");
        User user=new User();
        user.setId("12");
        user.setUsername("gdggfg");
        myRef.child("12").setValue(user);

    }
    public void readUser()
    {
        myRef = database.getReference("users");
        // Read from the database
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {



                for(DataSnapshot d : dataSnapshot.getChildren()) {
                    list.add(d.getKey());
                }
                Log.e("aaaaaaaaaaa","dfggfvv "+dataSnapshot.getRef());
                infalteUserData();
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }
    public void infalteUserData()
    {
        RecyclerView.LayoutManager la= new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rcUserList.setLayoutManager(la);
        UserListAdapter adapter=new UserListAdapter(getBaseContext(),list);
        rcUserList.setAdapter(adapter);
    }
    public   void moveToNextScreen(String id)
    {

        Intent intent=new Intent(MainActivity.this,MessagingActivity.class);
        intent.putExtra("id",id);
                startActivity(intent);
    }

}

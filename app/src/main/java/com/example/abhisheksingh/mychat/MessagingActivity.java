package com.example.abhisheksingh.mychat;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abhisheksingh.mychat.Utils.Constants;
import com.example.abhisheksingh.mychat.adapter.MessaginAdapter;
import com.example.abhisheksingh.mychat.pojo.Chat;
import com.example.abhisheksingh.mychat.pojo.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MessagingActivity extends AppCompatActivity {
    private static final int PICK_IMAGE = 11;
    private static final int PICK_VIDEO = 13;
    private static final int PICK_AUDIO = 12;
    String oppoonentId;
    private FirebaseDatabase database;
    private DatabaseReference myChatRef;

    private StorageReference mStorageRef;
    @BindView(R.id.etMessage)
    EditText etMessage;
    @BindView(R.id.rcMessage)
    RecyclerView rcMessage;
    String ownId;
    String groupKey;
    MessaginAdapter messaginAdapter;
    List<Chat> list = new ArrayList<>();
    MySharepref mySharepref;
    UploadTask uploadTask;
    private String ImageDecode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messaging);
        ButterKnife.bind(this);
        mySharepref = new MySharepref(this);
        mySharepref.getUserId();
        ownId = mySharepref.getUserId();

        Intent intent = getIntent();
        oppoonentId = intent.getStringExtra("id");
        String[] parts = ownId.split("_");
        int a = Integer.parseInt(parts[1]);
        String[] part = oppoonentId.split("_");
        int b = Integer.parseInt(part[1]);
        if (a < b) {
            groupKey = a + "_" + b;
        } else {
            groupKey = b + "_" + a;
        }

//        groupKey=
        database = FirebaseDatabase.getInstance();
        myChatRef = database.getReference("single_chat");
        mStorageRef = FirebaseStorage.getInstance().getReference().child("File");
        init();
        getData();
    }

    public void init() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        messaginAdapter = new MessaginAdapter(getBaseContext(), list);
        rcMessage.setLayoutManager(layoutManager);
        rcMessage.setAdapter(messaginAdapter);
        rcMessage.requestLayout();
    }

    @OnClick({R.id.ivSend, R.id.ivAdd})
    public void click(View view) {
        switch (view.getId()) {
            case R.id.ivAdd:
                showDailog();
                break;
            case R.id.ivSend:
                if (!etMessage.getText().toString().isEmpty()) {
                    Long tsLong = System.currentTimeMillis() / 1000;
                    String ts = tsLong.toString();
                    WriteMessage(ts);
                    etMessage.setText("");
                }
                break;
        }
    }

    public void WriteMessage(String ts) {

        Chat chat = new Chat();
        chat.setMessage(etMessage.getText().toString());
        chat.setMessagetype(Constants.TEXT);
        chat.setTimestamp(ts);
        chat.setUserid(ownId);
        chat.setVideoImage(" ");
        myChatRef.child(groupKey).push().setValue(chat);
    }

    public void getData() {
        myChatRef.child(groupKey).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Chat chat = dataSnapshot.getValue(Chat.class);
                list.add(chat);
                messaginAdapter.notifyDataSetChanged();
                rcMessage.scrollToPosition(list.size() - 1);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void showDailog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflate = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflate.inflate(R.layout.dilog_option, null);
        dialog.setContentView(view);
        TextView tvGallery = (TextView) dialog.findViewById(R.id.tvGallery);
        TextView tvAudio = (TextView) dialog.findViewById(R.id.tvAudio);
        //  TextView tvVideo = (EditText) dialog.findViewById(R.id.tvVideo);
        tvGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickClick();
                dialog.dismiss();
            }
        });
        tvAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                audioClick();
                dialog.dismiss();

            }
        });
        dialog.show();
    }
    public void pickClick() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }
        else {
            showMessage("NO DEVICE");
        }
    }
    public void audioClick() {
        Intent takeAudioIntent = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
        if (takeAudioIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takeAudioIntent, PICK_AUDIO);
        } else {
            showMessage("NO DEVICE");
        }
    }
    public void showMessage(String message)
    {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( resultCode == RESULT_OK)
        {
        if (requestCode == PICK_IMAGE  && null != data) {
            Uri uri = data.getData();
            uploadImage(uri);
        }
            if (requestCode == PICK_AUDIO  && null != data) {
                Uri uri = data.getData();
                uploadAudio(uri);
            }
        }
    }

    public void uploadImage(Uri uri) {
        mStorageRef.child(uri.getLastPathSegment()).putFile(uri).addOnCompleteListener(this,
                new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if (task.isSuccessful()) {
                            Log.e("hfhhjfdfdh", "Done");
                            // String message, String messagetype, String timestamp, String userid, String username, String videoImage
                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString();
                            Chat chat = new Chat("Image", Constants.IMAGE, ts, ownId, " ", task.getResult().getMetadata().getDownloadUrl()
                                    .toString());
                            myChatRef.child(groupKey).push().setValue(chat);

                        } else {
                            Log.e("hfhhjfdfdh", "Fail");
                        }

                    }
                });
    }
    public void uploadAudio(Uri uri) {
        mStorageRef.child(uri.getLastPathSegment()).putFile(uri).addOnCompleteListener(this,
                new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if (task.isSuccessful()) {
                            Log.e("hfhhjfdfdh", "Done");
                            // String message, String messagetype, String timestamp, String userid, String username, String videoImage
                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString();
                            Chat chat = new Chat("Audio", Constants.AUDIO, ts, ownId, " ", task.getResult().getMetadata().getDownloadUrl()
                                    .toString());
                            myChatRef.child(groupKey).push().setValue(chat);

                        } else {
                            Log.e("hfhhjfdfdh", "Fail");
                        }

                    }
                });
    }

    public void uploadFile(String path) {
        Uri file = Uri.fromFile(new File(path));

        uploadTask = mStorageRef.putFile(file);

// Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                Log.e("TAG", exception.toString());
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
            }
        });

//        InputStream stream = null;
//        try {
//            stream = new FileInputStream(new File(path));
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        uploadTask = mStorageRef.putStream(stream);
//        uploadTask.addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception exception) {
//                // Handle unsuccessful uploads
//                Log.e("TAG",exception.toString());
//            }
//        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//            @Override
//            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
//                Uri downloadUrl = taskSnapshot.getDownloadUrl();
//            }
//        });

    }
}


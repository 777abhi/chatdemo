package com.example.abhisheksingh.mychat.pojo;

/**
 * Created by Abhishek Singh on 3/28/2018.
 */

public class Chat {
    public  Chat()
    {}
    public Chat(String message, String messagetype, String timestamp, String userid, String username, String videoImage) {
        this.message = message;
        this.messagetype = messagetype;
        this.timestamp = timestamp;
        this.userid = userid;
        this.username = username;
        this.videoImage = videoImage;
    }

    private  String message;
    private  String messagetype;
    private  String timestamp;
    private  String userid;
    private  String username;
    private  String videoImage;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessagetype() {
        return messagetype;
    }

    public void setMessagetype(String messagetype) {
        this.messagetype = messagetype;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getVideoImage() {
        return videoImage;
    }

    public void setVideoImage(String videoImage) {
        this.videoImage = videoImage;
    }
}

package com.example.abhisheksingh.mychat;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Abhishek Singh on 3/28/2018.
 */

public class MySharepref  {
    SharedPreferences mSharedPreferences;
    SharedPreferences.Editor mEditor;
    private static final String MY_SHARE = "MY_SHARE";
    private static final String USER = "USER";
    public MySharepref(Context context) {
        mSharedPreferences = context.getSharedPreferences(MY_SHARE, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }
    public String getUserId() {
        return mSharedPreferences.getString(USER,"userID");
    }
    public void setUserId(String userId) {
        mEditor.putString(USER, userId);
        mEditor.commit();
    }
}
